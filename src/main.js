const name = "Regina";
const url = 'images/' + name.toLowerCase() + '.jpg';
console.log(url);

//-------------------------------

/*let html = `<a href=\"${url}\">${url}</a>`;
let html = `<article class=\"pizzaThumbnail\"> <a href=\"${url}\"> <img src=\"${url}\"/><section>${name}</section> </a> </article>`;
console.log(html);
document.querySelector('.pageContent').innerHTML = html;*/

//-------------------------------

/*const data = ['Regina', 'Napolitaine', 'Spicy'];
for (let i = 0; i < data.length; i++) {
    const name = data[i];
    const url = 'images/' + name.toLowerCase() + '.jpg';
    html += `<article class=\"pizzaThumbnail\"> <a href=\"${url}\"> <img src=\"${url}\"/><section>${name}</section> </a> </article>`;
}

document.querySelector('.pageContent').innerHTML = html;*/

//-------------------------------

const data = [
	{
		name: 'Regina',
		base: 'tomate',
		price_small: 6.5,
		price_large: 9.95,
		image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
	},
	{
		name: 'Napolitaine',
		base: 'tomate',
		price_small: 6.5,
		price_large: 8.95,
		image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
	},
	{
		name: 'Spicy',
		base: 'crème',
		price_small: 5.5,
		price_large: 8,
		image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300',
	}
];
let html = "";

// Sort by name
/*data_filtered.sort(function (a, b) {
    return a.name > b.name;
});*/

//Sort by price_small
/*data_filtered.sort(function (a, b) {
    if (a.price_small < b.price_small){
        return a.price_small;
    }else{
        return b.price_small;
    }
});*/

//Sort by price_small and if is egal by price_large
/*data_filtered.sort(function (a, b) {
    if (a.price_small == b.price_small){
        if (a.price_large < b.price_large){
            return a.price_large;
        }else{
            return b.price_large;
        }
    }else if (a.price_small < b.price_large){
        return a.price_large;
    }
});*/

//Filter by base = tomate
//const data_filtered = data.filter(pizza => pizza.base == 'tomate');

//Filter by price_small < 6 euros
//const data_filtered = data.filter(pizza => pizza.price_small < 6);

//Filter by 2 'i' in name
/*const data_filtered = data.filter(function(pizza){
    let j = 0;
    for (let i = 0; i < pizza.name.length; i++) {
        if (pizza.name.charAt(i) == 'i'){
            j++;
        }
    }
    if (j >= 2){
        return pizza;
    }
});*/

for (let i = 0; i < data_filtered.length; i++) {
    const name = data_filtered[i].name;
    html += `<article class="pizzaThumbnail"> 
    <a href="${data_filtered[i].image}"> 
        <img src="${data_filtered[i].image}\"/>
        <section>
            <h4>${name}</h4>
            <ul>
                <li>Prix petit format : ${data_filtered[i].price_small}</li>
                <li>Prix grand format : ${data_filtered[i].price_large}</li>
            </ul>
        </section> 
    </a> </article>`;
}

document.querySelector('.pageContent').innerHTML = html;


